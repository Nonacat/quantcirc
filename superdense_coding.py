from quantcirc.circuit import Circuit

import numpy as np


circuit = Circuit(5)

# Superdense coding
# Refer to below link to verify if it works:
# https://algassert.com/quirk#circuit={%22cols%22:[[1,1,%22H%22],[1,1,%22%E2%80%A2%22,1,%22X%22],[%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22],[%22Measure%22,%22Measure%22],[%22~msg%22],[%22Chance%22,%22Chance%22],[%22~enc%22],[1,%22%E2%80%A2%22,%22X%22],[%22%E2%80%A2%22,1,%22Z%22],[1,1,%22~send%22],[1,1,%22Swap%22,%22Swap%22],[1,1,1,%22~dec%22],[1,1,1,%22%E2%80%A2%22,%22X%22],[1,1,1,%22H%22],[1,1,1,%22Measure%22,%22Measure%22],[1,1,1,%22~msg%22],[1,1,1,%22Chance%22,%22Chance%22]],%22gates%22:[{%22id%22:%22~msg%22,%22name%22:%22message%22,%22matrix%22:%22{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}}%22},{%22id%22:%22~enc%22,%22name%22:%22encode%22,%22matrix%22:%22{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}}%22},{%22id%22:%22~send%22,%22name%22:%22send%22,%22matrix%22:%22{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}}%22},{%22id%22:%22~dec%22,%22name%22:%22decode%22,%22matrix%22:%22{{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}}%22}]}
circuit.H(2)
circuit.CX(4, 2)
circuit.CX(2, 1)
circuit.CZ(2, 0)
circuit.SWAP(3)
circuit.CX(4, 3)
circuit.H(3)


# Default input state is |0> for each qubit
print('----------------------------')
print('When |q_0> = 0 and |q_1> = 0')
print('----------------------------')
print()

states = circuit.calculate_result()
probabilities = Circuit.qubits_probabilities(states)

print('Inputs: ')
Circuit.print_input(circuit)

print('\nResult: ')
Circuit.print_states(states)

print('\nProbabilities: ')
Circuit.print_probabilities(probabilities)

print()
print('<><><><><><><><><><><><><><>')
print('><><><><><><><><><><><><><><')
print('<><><><><><><><><><><><><><>')
print()

# Set |q_0> and |q_1> states to |1> and recalculate results
print('----------------------------')
print('When |q_0> = 1 and |q_1> = 1')
print('----------------------------')
print()

circuit.set_qubit_state(0, alpha = 0, beta = 1)
circuit.set_qubit_state(1, alpha = 0, beta = 1)

states = circuit.calculate_result()
probabilities = Circuit.qubits_probabilities(states)

print('Inputs: ')
Circuit.print_input(circuit)

print('\nResult: ')
Circuit.print_states(states)

print('\nProbabilities: ')
Circuit.print_probabilities(probabilities)