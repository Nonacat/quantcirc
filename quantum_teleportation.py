from quantcirc.circuit import Circuit

import numpy as np


circuit = Circuit(3)
circuit.set_qubit_state(0, 0, 1) #|q_0> = |1>

# Quantum teleportation
# Refer to below link to verify if it works:
# https://algassert.com/quirk#circuit={%22cols%22:[[1,%22H%22],[1,%22%E2%80%A2%22,%22X%22],[%22%E2%80%A6%22,%22%E2%80%A6%22],[%22%E2%80%A6%22,%22%E2%80%A6%22],[%22%E2%80%A2%22,%22X%22],[%22H%22],[1,%22%E2%80%A2%22,%22X%22],[%22%E2%80%A2%22,1,%22Z%22],[1,1,%22Bloch%22],[1,1,%22~f7c0%22]],%22gates%22:[{%22id%22:%22~87lj%22,%22name%22:%22message%22,%22circuit%22:{%22cols%22:[[%22e^-iYt%22],[%22X^t%22]]}},{%22id%22:%22~f7c0%22,%22name%22:%22received%22,%22matrix%22:%22{{1,0},{0,1}}%22}],%22init%22:[1]}
circuit.H(1)
circuit.CX(2, 1)
circuit.CX(1, 0)
circuit.H(0)
circuit.CX(2, 1)
circuit.CZ(2, 0)

states = circuit.calculate_result()
probabilities = Circuit.qubits_probabilities(states)

print('Inputs: ')
Circuit.print_input(circuit)

print('\nResult: ')
Circuit.print_states(states)

print('\nProbabilities: ')
Circuit.print_probabilities(probabilities)