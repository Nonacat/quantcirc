from quantcirc.circuit import Circuit

import numpy as np


circuit = Circuit(5)

# Bell inequality test
# Refer to below link to verify if it works:
# https://algassert.com/quirk#circuit={%22cols%22:[[%22H%22],[%22%E2%97%A6%22,1,1,1,%22X%22],[%22X^-%C2%BC%22],[%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22],[%22~da85%22,%22~5s2n%22,1,%22~5s2n%22,%22~ahov%22],[1,%22H%22,1,%22H%22],[1,%22Measure%22,1,%22Measure%22],[%22X^%C2%BD%22,%22%E2%80%A2%22],[1,1,1,%22%E2%80%A2%22,%22X^%C2%BD%22],[%22Measure%22,1,1,1,%22Measure%22],[%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22,%22%E2%80%A6%22],[1,%22%E2%80%A2%22,%22X%22,%22%E2%80%A2%22],[%22%E2%80%A2%22,1,%22X%22],[1,1,%22X%22,1,%22%E2%80%A2%22],[1,1,%22Chance%22],[1,1,%22~q6e%22]],%22gates%22:[{%22id%22:%22~da85%22,%22name%22:%22Alice%22,%22matrix%22:%22{{1,0},{0,1}}%22},{%22id%22:%22~ahov%22,%22name%22:%22Bob%22,%22matrix%22:%22{{1,0},{0,1}}%22},{%22id%22:%22~5s2n%22,%22name%22:%22Referee%22,%22matrix%22:%22{{1,0},{0,1}}%22},{%22id%22:%22~q6e%22,%22name%22:%22Win?%22,%22matrix%22:%22{{1,0},{0,1}}%22}]}
circuit.H(0)

# Anticontrol CX
circuit.X(0)
circuit.CX(4, 0)
circuit.X(0)

# X^(-1/4) gate
X_minus_1_over_4 = np.matrix(
[
    [1/2 + complex(1/(2*np.sqrt(2)), -1/(2*np.sqrt(2))), 1/2 - complex(1/(2*np.sqrt(2)), -1/(2*np.sqrt(2)))],
    [1/2 - complex(1/(2*np.sqrt(2)), -1/(2*np.sqrt(2))), 1/2 + complex(1/(2*np.sqrt(2)), -1/(2*np.sqrt(2)))]
])

# X^(1/2) || sqrt(X)
X_1_over_2 = complex(1/2, 1/2) * np.matrix(
[
    [complex(1), complex(0, -1)],
    [complex(0, -1), complex(1)]
])

circuit.Cnk(X_minus_1_over_4, 0)

circuit.H(1)
circuit.H(3)

circuit.Cnk(X_1_over_2, 0, 1)
circuit.Cnk(X_1_over_2, 4, 3)

circuit.CCX(2, 1, 3)
circuit.CX(2, 0)
circuit.CX(2, 4)

states = circuit.calculate_result()
probabilities = Circuit.qubits_probabilities(states)

print('Inputs: ')
Circuit.print_input(circuit)

print('\nResult: ')
Circuit.print_states(states)

print('\nProbabilities: ')
Circuit.print_probabilities(probabilities)