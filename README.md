# QuantCirc
Quantum circuit simulator test

## Requirements
- numpy >= 1.18.4
- python >= 3.8.6

## Usage
Run algorithm script from terminal
```bash
$ python3 bell_inequality_test.py
$ python3 quantum_teleportation.py
$ python3 superdense_coding.py
```