from __future__ import annotations

from .qubit import Qubit
from .gate import QuantumGate
from .gate import QuantumGateMatrices

import numpy as np


class Circuit:
    '''
    Creates a quantum circuit with given number of qubits.
    Default states are |0>

    Static attributes
    -----------------
    instances: int
        Holds the number of circuit instances

    Attributes
    ----------
    _qubit_count: int
        Holds number of qubits in circuit
    _qubits: list
        Holds elements of type Qubit which represent
        individual qubits of circuit
    _gates: list
        Holds gates applied to the circuit.

    Methods
    -------
    X(qubit: int)
    Y(qubit: int)
    Z(qubit: int)
    H(qubit: int)
    S(qubit: int)
    T(qubit: int)
    def CNOT(x_qubit: int, control_qubit: int)
    def CX(x_qubit: int, control_qubit: int)
    def CY(y_qubit: int, control_qubit: int)
    def CZ(z_qubit: int, control_qubit: int)
    def CH(h_qubit: int, control_qubit: int)
    def CS(s_qubit: int, control_qubit: int)
    def CT(t_qubit: int, control_qubit: int)
    def SWAP(qubit: int)
    def CCX(x_qubit: int, control_qubit_1: int, control_qubit_2: int)
    def CNOT(x_qubit: int, control_qubit_1: int, control_qubit_2: int)
    def TOFF(x_qubit: int, control_qubit_1: int, control_qubit_2: int)
    def Cnk(gate: np.matrix, k_qubit: int, *control_qubits)
    def calculate_result()
    def print_qubits()
    '''

    instances: int = 0


    def __init__(self, qubit_count: int):
        '''
        Parameters
        ----------
        qubit_count: int
            Number of circuit's qubits

        Raises
        ------
        ValueError
            If number of qubits is not greater than 0
        '''

        if not qubit_count > 0:
            raise ValueError(
                'Number of qubits has to be greater than 0'
            )

        self._qubit_count: int = qubit_count
        self._qubits: list = list()
        self._gates: list = list()

        for _ in range(qubit_count):
            new_qubit = Qubit()
            self._qubits.append(new_qubit)
        
        Circuit.instances += 1

    
    def X(self, qubit: int) -> None:
        '''
        Apply X gate to circuit on given qubit

        Parameters
        ----------
        qubit: int
            Qubit to which the gate is applied
        '''

        assert qubit < self._qubit_count
        gate = QuantumGate(QuantumGateMatrices.X, qubit)
        self._gates.append(gate)


    def Y(self, qubit: int) -> None:
        '''
        Apply Y gate to the circuit on given qubit

        Parameters
        ----------
        qubit: int
            Qubit to which the gate is applied
        '''

        assert qubit < self._qubit_count
        gate = QuantumGate(QuantumGateMatrices.Y, qubit)
        self._gates.append(gate)


    def Z(self, qubit: int) -> None:
        '''
        Apply Z gate to the circuit on given qubit

        Parameters
        ----------
        qubit: int
            Qubit to which the gate is applied
        '''
        
        assert qubit < self._qubit_count
        gate = QuantumGate(QuantumGateMatrices.Z, qubit)
        self._gates.append(gate)


    def H(self, qubit: int) -> None:
        '''
        Apply H gate to the circuit on given qubit

        Parameters
        ----------
        qubit: int
            Qubit to which the gate is applied
        '''

        assert qubit < self._qubit_count
        gate = QuantumGate(QuantumGateMatrices.H, qubit)
        self._gates.append(gate)

    
    def S(self, qubit: int) -> None:
        '''
        Apply S gate to the circuit on given qubit

        Parameters
        ----------
        qubit: int
            Qubit to which the gate is applied
        '''
        
        assert qubit < self._qubit_count
        gate = QuantumGate(QuantumGateMatrices.S, qubit)
        self._gates.append(gate)


    def T(self, qubit: int) -> None:
        '''
        Apply T gate to the circuit on given qubit

        Parameters
        ----------
        qubit: int
            Qubit to which the gate is applied
        '''
        
        assert qubit < self._qubit_count
        gate = QuantumGate(QuantumGateMatrices.T, qubit)
        self._gates.append(gate)
    

    def CX(self, x_qubit: int, control_qubit: int) -> None:
        '''
        Apply CX (CNOT) gate to the circuit on given qubits.

        Parameters
        ----------
        x_qubit: int
            Qubit with X gate
        control_qubit: int
            Control qubit
        '''

        assert x_qubit < self._qubit_count
        assert control_qubit < self._qubit_count
        gate = QuantumGate.arbitrary_cnk_gate(
            QuantumGateMatrices.X,
            min(x_qubit, control_qubit),
            max(x_qubit, control_qubit),
            x_qubit, control_qubit
        )
        self._gates.append(gate)


    def CNOT(self, x_qubit: int, control_qubit: int) -> None:
        ''' Alias to CX '''
        self.CX(x_qubit, control_qubit)


    def CY(self, y_qubit: int, control_qubit: int) -> None:
        '''
        Apply CY gate to the circuit on given qubits.

        Parameters
        ----------
        y_qubit: int
            Qubit with Y gate
        control_qubit: int
            Control qubit
        '''

        assert y_qubit < self._qubit_count
        assert control_qubit < self._qubit_count
        gate = QuantumGate.arbitrary_cnk_gate(
            QuantumGateMatrices.Y,
            min(y_qubit, control_qubit),
            max(y_qubit, control_qubit),
            y_qubit, control_qubit
        )
        self._gates.append(gate)
    

    def CZ(self, z_qubit: int, control_qubit: int) -> None:
        '''
        Apply CZ gate to the circuit on given qubits.

        Parameters
        ----------
        z_qubit: int
            Qubit with Z gate
        control_qubit: int
            Control qubit
        '''

        assert z_qubit < self._qubit_count
        assert control_qubit < self._qubit_count
        gate = QuantumGate.arbitrary_cnk_gate(
            QuantumGateMatrices.Z,
            min(z_qubit, control_qubit),
            max(z_qubit, control_qubit),
            z_qubit, control_qubit
        )
        self._gates.append(gate)
    

    def CH(self, h_qubit: int, control_qubit: int) -> None:
        '''
        Apply CH gate to the circuit on given qubits.

        Parameters
        ----------
        h_qubit: int
            Qubit with H gate
        control_qubit: int
            Control qubit
        '''

        assert h_qubit < self._qubit_count
        assert control_qubit < self._qubit_count
        gate = QuantumGate.arbitrary_cnk_gate(
            QuantumGateMatrices.H,
            min(h_qubit, control_qubit),
            max(h_qubit, control_qubit),
            h_qubit, control_qubit
        )
        self._gates.append(gate)

    
    def CS(self, s_qubit: int, control_qubit: int) -> None:
        '''
        Apply CS gate to the circuit on given qubits.

        Parameters
        ----------
        s_qubit: int
            Qubit with S gate
        control_qubit: int
            Control qubit
        '''

        assert s_qubit < self._qubit_count
        assert control_qubit < self._qubit_count
        gate = QuantumGate.arbitrary_cnk_gate(
            QuantumGateMatrices.S,
            min(s_qubit, control_qubit),
            max(s_qubit, control_qubit),
            s_qubit, control_qubit
        )
        self._gates.append(gate)

    
    def CT(self, t_qubit: int, control_qubit: int) -> None:
        '''
        Apply CT gate to the circuit on given qubits.

        Parameters
        ----------
        t_qubit: int
            Qubit with T gate
        control_qubit: int
            Control qubit
        '''

        assert t_qubit < self._qubit_count
        assert control_qubit < self._qubit_count

        gate = QuantumGate.arbitrary_cnk_gate(
            QuantumGateMatrices.T,
            min(t_qubit, control_qubit),
            max(t_qubit, control_qubit),
            t_qubit, control_qubit
        )
        self._gates.append(gate)
    

    def SWAP(self, qubit: int) -> None:
        '''
        Swaps given qubit with previous one

        Parameters
        ----------
        first_qubit: int
            Qubit with X gate
        second_qubit: int
            Control qubit
        '''

        assert qubit < self._qubit_count
        assert qubit - 1 >= 0

        gate = QuantumGate(QuantumGateMatrices.SWAP, qubit - 1, qubit)
        self._gates.append(gate)

    
    def CCX(self, x_qubit: int, control_qubit_1: int, control_qubit_2: int) -> None:
        '''
        Apply CCX (CCNOT / TOFF) gate to the circuit on given qubits.
        Control qubits order is arbitrary

        Parameters
        ----------
        x_qubit: int
            Qubit with X gate
        control_qubit_1: int
            One of the control qubits
        control_qubit_2: int
            One of the control qubits
        '''

        assert x_qubit < self._qubit_count
        assert control_qubit_1 < self._qubit_count
        assert control_qubit_2 < self._qubit_count

        gate = QuantumGate.arbitrary_cnk_gate(
            QuantumGateMatrices.X,
            min(x_qubit, control_qubit_1, control_qubit_2),
            max(x_qubit, control_qubit_1, control_qubit_2),
            x_qubit, control_qubit_1, control_qubit_2
        )
        self._gates.append(gate)
    

    def CCNOT(self, x_qubit: int, control_qubit_1: int, control_qubit_2: int) -> None:
        ''' Alias to CX '''
        self.CCX(x_qubit, control_qubit_1, control_qubit_2)
    

    def TOFF(self, x_qubit: int, control_qubit_1: int, control_qubit_2: int) -> None:
        ''' Alias to CX '''
        self.CCX(x_qubit, control_qubit_1, control_qubit_2)


    def Cnk(self, gate: np.matrix, k_qubit: int, *control_qubits) -> None:
        '''
        Apply user defined gate to the circuit.
        Cnk means: control qubits n-times with k gate,
        where k is an arbitrary 1 qubit gate

        Parameters
        ----------
        gate: np.matrix
            Any one qubit matrix (2 x 2)
        k_qubit: int
            To which qubit apply the gate
        control_qubits: list
            Which qubits control the gate
        '''
        
        control_qubits = list(control_qubits)
        assert k_qubit < self._qubit_count
        for c in control_qubits:
            assert c < self._qubit_count
        rows = gate.shape[0]
        cols = gate.shape[1]
        lowest = min([k_qubit] + control_qubits)
        highest = max([k_qubit] + control_qubits)
        assert rows == cols

        gate = QuantumGate.arbitrary_cnk_gate(
            gate, lowest, highest,
            k_qubit, *control_qubits
        )
        self._gates.append(gate)


    def set_qubit(self, qubit_index: int, qubit: Qubit) -> None:
        '''
        Replaces the qubit with a new one

        Parameters
        ----------
        qubit_index: int
            Qubit index which we're replacing
        qubit: Qubit
            New qubit instance to replace current one
        '''

        assert qubit_index < self._qubit_count
        self._qubits[qubit_index] = qubit

    
    def set_qubit_state(
        self,
        qubit_index: int,
        alpha: complex, beta: complex
    ) -> None:
        '''
        Sets qubit's state to a new one

        Parameters
        ----------
        qubit_index: int
            Qubit index which state we're changing
        alpha: complex
            An amplitude of state |0>
        beta: complex
            An amplitude of state |1>
        '''

        assert qubit_index < self._qubit_count
        self._qubits[qubit_index].state = np.matrix(
        [
            [alpha],
            [beta]
        ])


    def calculate_result(self) -> list:
        '''
        Runs the circuit by applying all gates to states

        Returns
        -------
        Output states
        '''

        result: np.matrix = self._qubits[0].state
        for qubit in self._qubits[1:]:
            result = np.kron(qubit.state, result)
        
        for gate in self._gates:
            first_qubit = gate.lowest_qubit
            g = np.identity(1, dtype = complex)
            for _ in range(first_qubit):
                g = np.kron(g, np.identity(2))

            g = np.kron(gate.matrix, g)

            last_qubit = gate.highest_qubit
            for _ in range(last_qubit + 1, self._qubit_count):
                g = np.kron(np.identity(2), g)

            result = g * result

        return [item[0] for item in result.tolist()]


    @staticmethod
    def print_input(circuit: Circuit) -> None:
        ''' Prints input states '''

        for i in range(circuit._qubit_count):
            print('|q_{}> = '.format(i), end = '')
            state = circuit._qubits[i].state
            Circuit.print_states([state[0, 0], state[1, 0]], False)


    @staticmethod
    def qubits_probabilities(states: list):
        from math import log2
        qubits = int(log2(len(states)))

        probabilites = [[0, 0] for _ in range(qubits)]
        bit = 1
        for q in range(qubits):
            for v in range(2**qubits):
                if v & bit:
                    probabilites[q][1] += abs(states[v])**2
                else:
                    probabilites[q][0] += abs(states[v])**2
            bit = bit << 1
        
        return probabilites


    @staticmethod
    def print_states(states: list, do_format = True):
        from math import log2
        qubits = int(log2(len(states)))

        def parse_complex(number: complex):
            re = round(number.real, 4)
            im = round(number.imag, 4)

            r = False
            i = False

            template = ''

            if re == 0: pass
            elif int(re) == re:
                template += '{real:.0f}'
                r = True
            else:
                template += '{real:.4f}'
                r = True
            
            if im != 0:
                template += ' {} '.format('+-'[im < 0])

            if im == 0: pass
            elif int(im) == im:
                template += '{imag:.0f}i'
                i = True
            else:
                template += '{imag:.4f}i'
                i = True

            if r and i:
                template = '({})'.format(template)

            if template == '': return '0'
            return template.format(real = re, imag = abs(im))
        #END parse_complex

        max_width = max(
        [
            len(parse_complex(states[i]))
            for i in range(len(states))
        ])

        print(
            ' +{}'.format('\n' if do_format else ' ').join(
            [
                '{:{align}}|{:0{base}b}>'.format(
                    parse_complex(states[i]),
                    i,
                    base = qubits,
                    align = '>{}'.format(max_width) if do_format else ''
                )
                for i in range(len(states))
            ])
        )
    

    @staticmethod
    def print_probabilities(probabilites: list):
        for i, (s_0, s_1) in enumerate(probabilites):
            print(
                '|q_{}> =>\n'
                '    P(|0>) = {:.4f}\n'
                '    P(|1>) = {:.4f}'
                .format(
                    i, s_0, s_1
                )
            )
