import numpy as np


class Qubit:
    '''
    A class creating quantum circuit

    Methods
    -------
    set_state(state: tuple)
        Sets the new state as `state` which is a tuple of
        size 2 containing complex numbers
    '''

    def __init__(
        self,
        alpha: complex = complex(1),
        beta: complex = complex(0)
    ):
        '''
        Parameters
        ----------
        alpha: complex
            An amplitude of state |0>
        beta: complex
            An amplitude of state |1>
        
        Raises
        ------
        ValueError
            If sum of squares doesn't add up to 1 or state's
            tuple is not size of 2
        '''

        state = np.matrix(
        [
            [alpha],
            [beta]
        ])

        self._is_state_valid(state)
        self._state: np.matrix = np.matrix(state)

    
    def _is_state_valid(self, state: np.matrix) -> None:
        '''
        Method checks if the qubit's state is in proper form

        Parameters
        ----------
        state: np.matrix
            Tuple of size 2 containing qubit's state
        
        Raises
        ------
        ValueError
            If sum of squares doesn't add up to 1 or state's
            tuple is not size of 2
        '''

        if not state.shape == (2, 1):
            raise ValueError(
                'State matrix has to be of size (2, 1)'
            )

        # length(state) == 1 (+- float rounding)
        if not abs(abs(state[0])**2 + abs(state[1])**2) - 1 < 0.000002:
            raise ValueError(
                'State on Bloch\'s sphere has to '
                'have a length of 1.'
            )
    

    @property
    def state(self) -> np.matrix:
        return self._state.copy()

    @state.setter
    def state(self, state: np.matrix) -> None:
        '''
        Sets the qubit's state

        Parameters
        ----------
        state: tuple, optional
            Tuple of size 2 containing qubit's state
        
        Raises
        ------
        ValueError
            If sum of squares doesn't add up to 1 or state's
            tuple is not size of 2
        '''

        self._is_state_valid(state)
        self._state: np.matrix = np.matrix(state)
