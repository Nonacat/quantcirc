from __future__ import annotations

import numpy as np
import math


class QuantumGateMatrices:
    # X / NOT quantum gate
    X = np.matrix(
    [
        [0, 1],
        [1, 0]
    ], dtype = complex)

    # Y quantum gate
    Y = np.matrix(
    [
        [0            , complex(0, -1)],
        [complex(0, 1), 0             ]
    ], dtype = complex)

    # Z quantum gate
    Z = np.matrix(
    [
        [1,  0],
        [0, -1]
    ], dtype = complex)

    # H / Hadamard quantum gate
    H = 1 / np.sqrt(2) * np.matrix(
    [
        [1,  1],
        [1, -1]
    ], dtype = complex)

    S = np.matrix(
    [
        [1, 0            ],
        [0, complex(0, 1)]
    ], dtype = complex)

    T = np.matrix(
    [
        [1, 0                                ],
        [0, math.e**(complex(0, math.pi / 4))]
    ], dtype = complex)

    SWAP = np.matrix(
    [
        [1, 0, 0, 0],
        [0, 0, 1, 0],
        [0, 1, 0, 0],
        [0, 0, 0, 1]
    ], dtype = complex)
#END QuantumGateMatrices


class QuantumGate:
    def __init__(self, matrix: np.matrix, *qubits):
        rows = matrix.shape[0]
        cols = matrix.shape[1]

        assert rows == cols
        assert 2**(max(qubits) - min(qubits) + 1) == rows

        self._matrix: np.matrix = matrix.copy()
        self._qubits: list = list(qubits[:])


    @property
    def matrix(self):
        return self._matrix.copy()
    
    @property
    def qubits_count(self):
        return len(self._qubits)
    
    @property
    def lowest_qubit(self):
        return min(self._qubits)
    
    @property
    def highest_qubit(self):
        return max(self._qubits)


    @staticmethod
    def arbitrary_cnk_gate(
        single_qubit_gate_matrix: np.matrix,
        first_qubit_rel: int, last_qubit_rel: int,
        k_qubit: int, *control_qubits: list
    ) -> np.matrix:
        controls = list(control_qubits[:])
        swaps: list = list()

        # Helper function to move qubit to position 0
        # Saves needed swaps so we can apply them later
        def move_from_to(qubit, move_to):
            while qubit > move_to:
                i = last_qubit_rel
                m = np.identity(1)
                if qubit != last_qubit_rel:
                    while i > qubit:
                        m = np.kron(m, np.identity(2))
                        i -= 1
                
                m = np.kron(m, QuantumGateMatrices.SWAP)
                for j in range(len(controls)):
                    if controls[j] == qubit:
                        controls[j] -= 1
                        break
                    if controls[j] == qubit - 1:
                        controls[j] += 1
                        break
                qubit -= 1

                i -= 2
                while i >= first_qubit_rel:
                    m = np.kron(m, np.identity(2))
                    i -= 1
                swaps.append(m)
            
            return qubit

        # Move k gate to first position
        k_qubit = move_from_to(k_qubit, first_qubit_rel)
        
        # Move control qubits after the k gate
        controls_moved: list = list()

        controls.sort(reverse = True)
        control_id = 1

        while len(controls) > 0:
            current_control = controls.pop()
            current_control = move_from_to(current_control, first_qubit_rel + control_id)
            
            controls_moved.append(current_control)
            control_id += 1

        # Generate matrix with k gate size
        result = np.identity(
            2**(last_qubit_rel - first_qubit_rel + 1),
            dtype = complex
        )

        # Apply swaps to get to known gate matrix
        for s in swaps:
            result = result * s

        custom_ck = np.matrix(1)
        for _ in range(last_qubit_rel - first_qubit_rel - len(controls_moved)):
            custom_ck = np.kron(custom_ck, np.identity(2))
        
        # n - number of control qubits
        # k - one qubit gate (e.g. X)
        cnk_matrix = np.identity(2**(len(controls_moved) + 1), dtype = complex)
        cnk_matrix[-2:, -2:] = single_qubit_gate_matrix.copy()

        custom_ck = np.kron(custom_ck, cnk_matrix)
        result = result * custom_ck

        # Swap everything back where it was
        for s in reversed(swaps):
            result = result * s

        # Create and return the generated gate
        return QuantumGate(
            result, *[i for i in range(first_qubit_rel, last_qubit_rel + 1)]
        )

    
    def __repr__(self) -> str:
        return self._matrix.copy().__str__()
